package kshrd.org.homeworkthymeleaf.Controllers;

import kshrd.org.homeworkthymeleaf.services.FormService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewPageController {

    @GetMapping("/ViewPage")
    public String viewPage(){
        return "ViewPage";
    }

    @RequestMapping("/view/{id}")
    public String getView(@PathVariable("id") int id, Model model){
        model.addAttribute("view", FormService.getList(id));
        return "ViewPage";
    }

    @GetMapping("/view/Home")
    public String goHome(){
        return "redirect:/Home";
    }

}
