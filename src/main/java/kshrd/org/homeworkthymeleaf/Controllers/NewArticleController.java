package kshrd.org.homeworkthymeleaf.Controllers;

import kshrd.org.homeworkthymeleaf.model.Form;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

@Controller
public class NewArticleController{

    public static ArrayList<Form> list = new ArrayList<>();

    @GetMapping("/NewArticle")
    public String newArticle(){

        return "NewArticle";
    }

    @PostMapping("/upload-data/NewArticle")
    public String uploadData(@RequestParam("file") MultipartFile file, @ModelAttribute Form form){
        form.setImage(file.getOriginalFilename());
        form.setId(list.size()+1);
        list.add(form);
        try{
            String fileName = file.getOriginalFilename();
            Path path = Paths.get("src/main/resources/files/"+ fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        }catch (IOException e){
            e.printStackTrace();
        }
        return "redirect:/NewArticle";
    }
}
