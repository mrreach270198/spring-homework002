package kshrd.org.homeworkthymeleaf.Controllers;

import kshrd.org.homeworkthymeleaf.model.Form;
import kshrd.org.homeworkthymeleaf.services.FormService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Controller
public class UpdateController {

    String oldImage;
    String fileName;
    int getId;

    @GetMapping("/UpdateForm")
    public String getHome(){
        return "redirect:/Home";
    }

    @PostMapping("/upload-data/UpdateForm")
    public String uploadData(@RequestParam("file") MultipartFile file, @ModelAttribute Form form){
        if (fileName.equals("")){
            fileName = oldImage;
        }
        fileName = file.getOriginalFilename();
        form.setImage(file.getOriginalFilename());
        System.out.println(form);
        System.out.println("Old img" + oldImage);
        System.out.println("New img"+fileName);
        FormService.updateItem(getId, form.getId(), form.getTitle(), form.getDescription(), fileName);
        System.out.println("List "+ NewArticleController.list);
        try{
            Path path = Paths.get("src/main/resources/files/"+ fileName);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);


        }catch (IOException e){

        }
        return "redirect:/UpdateForm";
    }

    @GetMapping("/edit/{id}")
    public String updateView(@PathVariable("id") int id, Model model){
        getId = id;
        oldImage = FormService.getImage(oldImage, id);
        model.addAttribute("update", FormService.getList(id));
        return "UpdateForm";
    }

    @GetMapping("/edit/Home")
    public String goHome(){
        return "redirect:/Home";
    }
}
