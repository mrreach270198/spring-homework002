package kshrd.org.homeworkthymeleaf.Controllers;

import kshrd.org.homeworkthymeleaf.services.FormService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class HomeController {

    String text = "";

    @GetMapping
    public String home(Model model){
        model.addAttribute("myText", text);
        return "Home";
    }

//    @GetMapping ("/Home")
//    public String goHome() {
//        return "Home";
//    }

    @GetMapping("/Home")
    public String getData(Model model){
        model.addAttribute("list", NewArticleController.list);
        return "Home";
    }

    @GetMapping("/delete/{id}")
    public String deleteRow(@PathVariable("id") int id){
        FormService.deleteItem(id);
        return "redirect:/Home";
    }
}
