package kshrd.org.homeworkthymeleaf.services;

import kshrd.org.homeworkthymeleaf.Controllers.NewArticleController;
import kshrd.org.homeworkthymeleaf.model.Form;
import org.springframework.stereotype.Service;


@Service
public class FormService {

    public static Form getList(int id){
        for (int i=0; i<NewArticleController.list.size(); i++){
            if (id == NewArticleController.list.get(i).getId()){
                return NewArticleController.list.get(i);
            }
        }
        return null;
    }

    public static void deleteItem(int id){
        for (int i=0; i<NewArticleController.list.size(); i++){
            if (id == NewArticleController.list.get(i).getId()){
                NewArticleController.list.remove(NewArticleController.list.get(i));
                System.out.println("Delete success");
            }
        }
    }

    public static void updateItem(int id, int newId, String title, String des, String img){
        for (int i=0; i<NewArticleController.list.size(); i++){
            if (id == NewArticleController.list.get(i).getId()){
                NewArticleController.list.get(i).setId(newId);
                NewArticleController.list.get(i).setTitle(title);
                NewArticleController.list.get(i).setDescription(des);
                NewArticleController.list.get(i).setImage(img);
                System.out.println("Update success");
            }
        }
    }

    public static String getImage(String oldImg, int id){
        for (int i=0; i<NewArticleController.list.size(); i++){
            if (id == NewArticleController.list.get(i).getId()){
               oldImg = NewArticleController.list.get(i).getImage();
            }
        }

        return oldImg;
    }
}
